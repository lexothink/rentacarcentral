<!doctype html>
<html lang="en">

<head>
    <?php include('include/head.php'); ?>
</head>

<body>
    <!--================Header Area =================-->
    <header class="header_area">
        <?php include('include/nav.php'); ?>
    </header>
    <!--================End Header Area =================-->

    <!--================Content Area =================-->
    <?php
    $folder = isset($_GET['folder']) ? $_GET['folder'] : 'pages/';
    $page = isset($_GET['page']) ? $_GET['page'] : 'home';
    require_once($folder . $page . '.php');
    ?>
    <!--================End Content Area =================-->

    <!--================ footer Area  =================-->
    <footer class="footer-area section_gap">
        <?php include('include/footer.php'); ?>
    </footer>
    <!--================ End footer Area  =================-->

    <!-- Optional JavaScript -->
    <?php include('include/script.php'); ?>
</body>

</html>